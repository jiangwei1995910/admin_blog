<?php

/*
 * REMOTE_ADDR：是和服务器直接"握手"的IP。
HTTP_CLIENT_IP：代理服务器添加的 HTTP 头，存放客户端真实IP。
HTTP_X_FORWARDED_FOR：代理服务器添加的HTTP头，存放真实ip和各级代理ip。格式为X-Forwarded-For: client1, proxy1, proxy2。
 *
 */
$REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];                    //连接IP

$HTTP_X_FORWARDED_FOR = $_SERVER['HTTP_X_FORWARDED_FOR'];             //经过透明代理的原IP

$HTTP_CLIENT_IP = $_SERVER['HTTP_CLIENT_IP'];
include_once "./ip2region/Ip2Region.class.php";

$ip2regionObj = new Ip2Region('./ip2region/ip2region.db');

$data = $ip2regionObj->btreeSearch($REMOTE_ADDR);

echo json_encode(array(
    'HTTP_CLIENT_IP' => $HTTP_CLIENT_IP,
    'HTTP_X_FORWARDED_FOR' => $HTTP_X_FORWARDED_FOR,
    'REMOTE_ADDR' => $REMOTE_ADDR,
    'LOCATION' =>$data


));